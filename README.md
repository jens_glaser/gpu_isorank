# gpu_isorank

gpu_isorank is a GPU implementation of the IsoRank algorithm for graph alignment.
It uses nvGRAPH, which is available in CUDA 9.
