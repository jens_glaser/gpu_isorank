#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include "isorank.cuh"
#include <cuda_runtime.h>

#include <vector>
#include <queue>
#include <set>
#include <tuple>

#include <iostream>

typedef pybind11::array_t<float, pybind11::array::f_style | pybind11::array::forcecast> pyarr_d;

pybind11::array_t<float> isorank(pyarr_d a, pyarr_d b, float alpha = 0.85)
    {
    pybind11::buffer_info info_a = a.request();
    pybind11::buffer_info info_b = b.request();

    float *data_a = reinterpret_cast<float*>(info_a.ptr);
    float *data_b = reinterpret_cast<float*>(info_b.ptr);
    unsigned int n_a = info_a.shape[0];
    unsigned int m_a = info_a.shape[1];

    unsigned int n_b = info_b.shape[0];
    unsigned int m_b = info_b.shape[1];

    static int last_na = -1;
    static int last_nb = -1;

    bool reallocate = false;
    if (n_a != last_na || n_b != last_nb)
        {
        last_na = n_a;
        last_nb = n_b;
        reallocate = true;
        }

    if (n_a != m_a)
        throw std::runtime_error("matrix a needs to be a square matrix.\n");
    if (n_b != m_b)
        throw std::runtime_error("matrix b needs to be a square matrix.\n");
    if (n_a != n_b)
        throw std::runtime_error("matrix a and b need to be of same size.\n");

    static float *d_pr; // page rank vector of product graph

    if (reallocate)
        check_cuda(cudaMalloc((void **) &d_pr, sizeof(int)*n_a*n_b));

    // find optimal matching
    gpu_isorank(
        n_a,
        n_b,
        data_a,
        data_b,
        alpha,
        d_pr);

    // copy to host
    auto result = pybind11::array_t<float>(n_a*n_b);

    float *buf = reinterpret_cast<float *>(result.request().ptr);

    check_cuda(cudaMemcpy(buf, d_pr, sizeof(float)*n_a*n_b,cudaMemcpyDeviceToHost));

    #if 0
    check_cuda(cudaFree(d_pr));
    #endif

    return result;
    }

float dist(pyarr_d a, pyarr_d b, float alpha = 0.85)
    {
    pybind11::buffer_info info_a = a.request();
    pybind11::buffer_info info_b = b.request();

    unsigned int n_a = info_a.shape[0];
    unsigned int n_b = info_b.shape[0];

    float *data_a = reinterpret_cast<float*>(info_a.ptr);
    float *data_b = reinterpret_cast<float*>(info_b.ptr);

    // if one of the matrices is trivial (no edges), skip isorank computation (it would fail)
    int n_edges_a = 0;
    for (int i = 0; i < n_a; ++i)
        for (int j = 0; j < n_a; ++j)
            if (i!=j)
                n_edges_a += data_a[j + i*n_a];

    int n_edges_b = 0;
    for (int i = 0; i < n_b; ++i)
        for (int j = 0; j < n_b; ++j)
            if (i!=j)
                n_edges_b += data_b[j + i*n_a];

    int p[n_b];
    int pt[n_a];

    if (!n_edges_a || !n_edges_b)
        {
        for (int i = 0; i < n_a; ++i)
            p[i] = i;
        }
    else
        {
        // compute isorank scores
        pybind11::buffer_info info_pr = isorank(a,b,alpha).request();
        float *pr = reinterpret_cast<float*>(info_pr.ptr);

        #if 1
        // find an optimal permutation from b to a using a greedy BFS algorithm
        // see supporting info to Long and Ferguson 2014
        typedef std::pair<float, std::pair<int, int> > element_t;
        std::vector<element_t> pairs;
        for (unsigned int i = 0; i < n_a; ++i)
            for (unsigned int j = 0; j < n_b; ++j)
                pairs.push_back(std::make_pair(pr[i + j*n_a],std::make_pair(i,j)));

        std::sort(pairs.begin(), pairs.end(), std::greater<element_t>());

        std::set<std::pair<element_t, int>, std::greater<std::pair<element_t, int> > > component;
        std::set<std::pair<int,element_t>, std::greater<std::pair<int, element_t> > > tie_set;

        auto visited_a = std::vector<bool>(n_a,false);
        auto visited_b = std::vector<bool>(n_b,false);

        for (auto max_it = pairs.begin(); max_it != pairs.end(); max_it++)
            {
            auto queue = std::queue<std::pair<element_t, int> >();
            queue.push(std::make_pair(*max_it,0) );

            // find the connected component
            component.clear();

            while (!queue.empty())
                {
                auto el = queue.front();
                queue.pop();

                int i = el.first.second.first;
                int j = el.first.second.second;

                if (!visited_a[i] && !visited_b[j])
                    {
                    visited_a[i] = true;
                    visited_b[j] = true;

                    component.insert(el);

                    for (unsigned int neigh_i = 0; neigh_i < n_a; ++neigh_i)
                        if (data_a[neigh_i + i*n_a] != 0.0)
                            {
                            // record node degree for tie breaking
                            int c_i = 0;
                            for (int k = 0; k < n_a; ++k)
                                if (data_a[k + neigh_i*n_a] != 0.0)
                                    c_i++;

                            for (unsigned int neigh_j = 0; neigh_j < n_b; ++neigh_j)
                                if (data_b[neigh_j + j*n_b] != 0.0)
                                        {
                                        int c_j = 0;
                                        for (int k = 0; k < n_a; ++k)
                                            if (data_b[k + neigh_j*n_b] != 0.0)
                                                c_j++;

                                        queue.push(std::make_pair(
                                            std::make_pair(pr[neigh_i + neigh_j*n_a],std::make_pair(neigh_i,neigh_j)),
                                            c_i+c_j));
                                        }
                            }
                    }
                } // end while queue has elements

            // break ties in this connected component
            for (auto it = component.begin(); it != component.end(); ++it)
                {
                auto itj = it;
                tie_set.clear();
                tie_set.insert(std::make_pair(it->second,it->first));
                while (++itj != component.end() && itj->first.first == it->first.first)
                    {
                    tie_set.insert(std::make_pair(itj->second,itj->first));
                    }
                it = --itj;
                for (auto itk = tie_set.begin(); itk != tie_set.end(); itk++)
                    {
                    std::pair<int, int> ij = itk->second.second;
                    p[ij.first] = ij.second;
                    }
                }
            }
        #else
        // find an optimal permutation from b to a using greedy algorithm
        typedef std::pair<float, std::pair<int, int> > element_t;
        auto queue = std::priority_queue<element_t>();
        auto visited_a = std::set<int>();
        auto visited_b = std::set<int>();

        for (unsigned int i = 0; i < n_a; ++i)
            for (unsigned int j = 0; j < n_b; ++j)
                queue.push(std::make_pair(pr[i + j*n_a],std::make_pair(i,j)));

        while (!queue.empty())
            {
            // get the remaining element with the highest score
            auto el = queue.top();
            queue.pop();

            int i = el.second.first;
            int j = el.second.second;

            auto iti = visited_a.find(i);
            auto itj = visited_b.find(j);

            if (iti == visited_a.end() && itj == visited_b.end())
                {
                p[i] = j;
                visited_a.insert(i);
                visited_b.insert(j);
                }
            }
        #endif
        }

    // compute metric using Frobenius norm
    for (int i = 0; i < n_a; ++i)
        pt[p[i]] = i;

    float fro_1 = 0.0;
    for (unsigned int i = 0; i < n_b; ++i)
        {
        for (unsigned int j = 0; j < n_b; ++j)
            {
            float d = data_a[pt[j]+pt[i]*n_a] - data_b[j+i*n_b];
            fro_1 += fabs(d);
            }
        }

    return fro_1/(n_a*(n_a-1));
    }


pybind11::array_t<float> pdist2(std::vector<pybind11::array_t<float> > p1, std::vector<pybind11::array_t<float> > p2, float alpha)
    {
    size_t N1 = p1.size();
    size_t N2 = p2.size();

    auto result = pybind11::array_t<float>(N1*N2);
    float *buf = reinterpret_cast<float *>(result.request().ptr);

    // ensuring good parallelization
    if(N1 == 1)
        {
            for(size_t j = 0; j < N2; ++j)
                buf[j] = dist(p1[0],p2[j],alpha);
        }
    else if(N2 == 1)
        {
            for(size_t i = 0; i < N1; ++i)
                buf[i] = dist(p1[i], p2[i], alpha);

        }
    else
        {
        for(size_t i = 0; i < N1; ++i)
            for(size_t j = 0; j < N2; ++j)
                buf[j+i*N1] = dist(p1[i], p2[j], alpha);
        }
    return result;
    }

