#define check_cuda(a) \
    {\
    cudaError_t status = (a);\
    if ((int)status != cudaSuccess)\
        {\
        printf("CUDA ERROR %d in file %s line %d: %s\n",status,__FILE__,__LINE__,cudaGetErrorString(status));\
        throw std::runtime_error("Error during graph matching");\
        }\
    }

void gpu_isorank(
    unsigned int n_a,
    unsigned int n_b,
    float *data_a,
    float *data_b,
    float alpha,
    float *d_pr);
