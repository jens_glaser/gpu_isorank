#include <nvgraph.h>
#include <cusparse.h>

#include <thrust/reduce.h>
#include <thrust/fill.h>
#include <thrust/device_ptr.h>
#include <thrust/iterator/discard_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/counting_iterator.h>

#include <thrust/random.h>

#include "isorank.cuh"

#define check_nvgraph(a) \
    {\
    nvgraphStatus_t status = (a);\
    if ((int)status != NVGRAPH_STATUS_SUCCESS)\
        {\
        printf("nvgraph ERROR %d in file %s line %d\n",status,__FILE__,__LINE__);\
        throw std::runtime_error("Error during graph matching");\
        }\
    }

#define check_cusparse(a) \
    {\
    cusparseStatus_t status = (a);\
    if ((int)status != CUSPARSE_STATUS_SUCCESS)\
        {\
        printf("cusparse ERROR %d in file %s line %d\n",status,__FILE__,__LINE__);\
        throw std::runtime_error("Error during graph matching");\
        }\
    }

// convert a linear index to a row index using row-major
template <typename T>
struct linear_index_to_row_index : public thrust::unary_function<T,T>
    {
    T C; // number of columns

    __host__ __device__
    linear_index_to_row_index(T C) : C(C) {}

    __host__ __device__ T operator()(T i)
        {
        return i / C;
        }
    };

__global__ void reset_diagonals(
    unsigned int n,
    float *d_a,
    float *d_b)
    {
    unsigned int tidx = blockIdx.x*blockDim.x + threadIdx.x;

    if (tidx > n)
        return;

    d_a[tidx+n*tidx] = 0.0;
    d_b[tidx+n*tidx] = 0.0;
    }

__global__ void tensorproduct(
    unsigned int n_a,
    unsigned int n_b,
    float *d_a,
    float *d_b,
    float *d_row_sum_a,
    float *d_row_sum_b,
    float *d_product)
    {
    unsigned int tidx_x = blockIdx.x*blockDim.x+threadIdx.x;
    unsigned int tidx_y = blockIdx.y*blockDim.y+threadIdx.y;

    if (tidx_x >= n_a*n_b || tidx_y >= n_a*n_b)
        return;

    unsigned int xa = tidx_x % n_a;
    unsigned int ya = tidx_y % n_a;
    unsigned int xb = tidx_x / n_a;
    unsigned int yb = tidx_y / n_a;

    // row major
    unsigned int a = d_a[xa+ya*n_a];
    unsigned int b = d_b[xb+yb*n_b];

    if (a != 0.0 && b != 0.0)
        d_product[tidx_y+n_a*n_b*tidx_x] = 1.0f/(d_row_sum_a[ya]*d_row_sum_b[yb]);
    else
        d_product[tidx_y+n_a*n_b*tidx_x] = 0.0;
    }

__global__ void compute_bookmark(
    unsigned int n,
    float *d_row_sum,
    float *d_bookmark)
    {
    unsigned int tidx = blockIdx.x*blockDim.x+threadIdx.x;

    if (tidx >= n)
        return;

    d_bookmark[tidx] = (d_row_sum[tidx] != 0.0) ? 0.0 : 1.0;
    }

void gpu_isorank(
    unsigned int n_a,
    unsigned int n_b,
    float *data_a,
    float *data_b,
    float alpha,
    float *d_pr)
    {
    static int last_n_a = -1;
    static int last_n_b = -1;

    bool reallocate = false;
    if (last_n_a != n_a || last_n_b != n_b)
        {
        last_n_a = n_a;
        last_n_b = n_b;
        reallocate = true;
        }

    static nvgraphHandle_t nvgraphH;
    static cusparseHandle_t cusparseH;

    static float *d_a, *d_b;
    static float *d_product;

    if (reallocate)
        {
        check_cuda(cudaMalloc((void **)&d_a, n_a*n_a*sizeof(float)));
        check_cuda(cudaMalloc((void **)&d_b, n_b*n_b*sizeof(float)));
        check_cuda(cudaMalloc((void **)&d_product, n_a*n_a*n_b*n_b*sizeof(float)));
        }

    // transfer host data
    check_cuda(cudaMemcpy(d_a, data_a, n_a*n_a*sizeof(float),cudaMemcpyHostToDevice));
    check_cuda(cudaMemcpy(d_b, data_b, n_b*n_b*sizeof(float),cudaMemcpyHostToDevice));

    unsigned int block_size = 256;
    reset_diagonals<<<n_a/block_size+1, block_size>>>(n_a, d_a, d_b);
    check_cuda(cudaGetLastError());

    unsigned tile_dim = 16;
    dim3 threads(tile_dim, tile_dim,1);
    dim3 grid((n_a*n_b)/tile_dim+1,(n_a*n_b)/tile_dim+1,1);

    // sum rows of matrix a
    static float *d_row_sum_a;
    if (reallocate)
        check_cuda(cudaMalloc((void **)&d_row_sum_a, n_a*sizeof(float)));

    thrust::device_ptr<float> row_sum_a(d_row_sum_a);
    thrust::device_ptr<float> a(d_a);

    auto row_it = thrust::make_transform_iterator(thrust::counting_iterator<int>(0), linear_index_to_row_index<int>(n_a));
    thrust::reduce_by_key(row_it,
        row_it + n_a*n_a,
        a,
        thrust::make_discard_iterator(),
        row_sum_a,
        thrust::equal_to<int>(),
        thrust::plus<float>());

    // sum rows of matrix b
    static float *d_row_sum_b;
    if (reallocate)
        check_cuda(cudaMalloc((void **)&d_row_sum_b, n_b*sizeof(float)));

    thrust::device_ptr<float> row_sum_b(d_row_sum_b);
    thrust::device_ptr<float> b(d_b);

    row_it = thrust::make_transform_iterator(thrust::counting_iterator<int>(0), linear_index_to_row_index<int>(n_b));
    thrust::reduce_by_key(row_it,
        row_it + n_b*n_b,
        b,
        thrust::make_discard_iterator(),
        row_sum_b,
        thrust::equal_to<int>(),
        thrust::plus<float>());

    // compute product matrix
    tensorproduct<<<grid, threads>>>(n_a, n_b, d_a, d_b, d_row_sum_a, d_row_sum_b, d_product);
    check_cuda(cudaGetLastError());

    // sum the rows of the product matrix
    static float *d_row_sum;
    if (reallocate)
        check_cuda(cudaMalloc((void **)&d_row_sum, n_a*n_b*sizeof(float)));

    thrust::device_ptr<float> row_sum(d_row_sum);
    thrust::device_ptr<float> product(d_product);

    row_it = thrust::make_transform_iterator(thrust::counting_iterator<int>(0), linear_index_to_row_index<int>(n_a*n_b));
    thrust::reduce_by_key(row_it,
        row_it + n_a*n_a*n_b*n_b,
        product,
        thrust::make_discard_iterator(),
        row_sum,
        thrust::equal_to<int>(),
        thrust::plus<float>());

    // normalize the matrix and determine dangling vertices
    static float *d_bookmark;
    if (reallocate)
        check_cuda(cudaMalloc((void **)&d_bookmark, n_a*n_b*sizeof(float)));

    block_size = 256;
    compute_bookmark<<<(n_a*n_b)/block_size+1, block_size>>>(n_a*n_b, d_row_sum, d_bookmark);
    check_cuda(cudaGetLastError());

    static int *d_nnz;
    static float *d_csc_val;
    static int *d_csc_row_ind;
    static int *d_csc_col_ptr;

    if (reallocate)
        check_cuda(cudaMalloc((void **)&d_nnz, n_a*n_b*sizeof(int)));

    // convert into sparse CSC matrix
    if (reallocate)
        check_cusparse(cusparseCreate(&cusparseH));

    static cusparseMatDescr_t descr_A;
    if (reallocate)
        check_cusparse(cusparseCreateMatDescr(&descr_A));

    int nnz;
    check_cusparse(cusparseSnnz(cusparseH, CUSPARSE_DIRECTION_COLUMN, n_a*n_b, n_a*n_b, descr_A, d_product,
        n_a*n_b, d_nnz, &nnz));

    if (reallocate)
        {
        check_cuda(cudaMalloc((void **)&d_csc_val, n_a*n_a*n_b*n_b*sizeof(float)));
        check_cuda(cudaMalloc((void **)&d_csc_row_ind, n_a*n_a*n_b*n_b*sizeof(int)));
        check_cuda(cudaMalloc((void **)&d_csc_col_ptr, (n_a*n_b+1)*sizeof(int)));
        }

    check_cusparse(cusparseSdense2csc(cusparseH, n_a*n_b, n_a*n_b, descr_A, d_product, n_a*n_b,
        d_nnz, d_csc_val, d_csc_row_ind, d_csc_col_ptr));

    // setup graph
    nvgraphCSCTopology32I_t CSC_input;
    nvgraphGraphDescr_t G;
    if (reallocate)
        check_nvgraph(nvgraphCreate(&nvgraphH));

    check_nvgraph(nvgraphCreateGraphDescr(nvgraphH, &G));

    CSC_input = (nvgraphCSCTopology32I_t) malloc(sizeof(struct nvgraphCSCTopology32I_st));

    CSC_input->nvertices = n_a*n_b;
    CSC_input->nedges = nnz;
    CSC_input->destination_offsets = d_csc_col_ptr;
    CSC_input->source_indices = d_csc_row_ind;

    // set graph connectivity and structure
    cudaDataType_t edge_dimT = CUDA_R_32F;
    int vert_sets = 2;
    int edge_sets = 1;
    cudaDataType_t vertex_dimT[vert_sets];
    void *vertex_dim[vert_sets];

    // allocate page rank vector
    vertex_dimT[0] = CUDA_R_32F;
    vertex_dimT[1] = CUDA_R_32F;
    vertex_dim[0] = (void *) d_bookmark;
    vertex_dim[1] = (void *) d_pr;

    check_nvgraph(nvgraphSetGraphStructure(nvgraphH, G, (void *)CSC_input, NVGRAPH_CSC_32));
    check_nvgraph(nvgraphAllocateVertexData(nvgraphH, G, vert_sets, vertex_dimT));
    check_nvgraph(nvgraphAllocateEdgeData(nvgraphH, G, edge_sets, &edge_dimT));
    check_nvgraph(nvgraphSetVertexData(nvgraphH, G, vertex_dim[0], 0));
    check_nvgraph(nvgraphSetVertexData(nvgraphH, G, vertex_dim[1], 1));
    check_nvgraph(nvgraphSetEdgeData(nvgraphH, G, (void *) d_csc_val, 0));

    // compute pagerank
    float alpha_p = alpha;
    check_nvgraph(nvgraphPagerank(nvgraphH, G, 0, &alpha_p, 0, 0, 1, 0.0f, 0));

    // download result
    check_nvgraph(nvgraphGetVertexData(nvgraphH, G, vertex_dim[1], 1));

    nvgraphDestroyGraphDescr(nvgraphH,G);
    free(CSC_input);

    #if 0
    check_cuda(cudaFree(d_a));
    check_cuda(cudaFree(d_b));
    check_cuda(cudaFree(d_product));

    check_cuda(cudaFree(d_nnz));
    check_cuda(cudaFree(d_csc_val));
    check_cuda(cudaFree(d_csc_row_ind));
    check_cuda(cudaFree(d_csc_col_ptr));
    check_cuda(cudaFree(d_row_sum_a));
    check_cuda(cudaFree(d_row_sum_b));
    check_cuda(cudaFree(d_row_sum));
    check_cuda(cudaFree(d_bookmark));

    check_nvgraph(nvgraphDestroy(nvgraphH));
    #endif

    #if 0
    check_cusparse(cusparseDestroyMatDescr(descr_A));
    check_cusparse(cusparseDestroy(cusparseH));
    #endif
    }
