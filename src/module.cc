#include <pybind11/pybind11.h>
#include "isorank.h"

PYBIND11_MODULE(isorank,m)
    {
    m.doc() = "CUDA implementation of isorank score for graph alignment using nvGRAPH";

    m.def("isorank", &isorank, pybind11::arg("a"), pybind11::arg("b"), pybind11::arg("alpha") = 0.85);
    m.def("dist", &dist, pybind11::arg("a"), pybind11::arg("b"), pybind11::arg("alpha") = 0.85);
    m.def("pdist2", &pdist2, pybind11::arg("a"), pybind11::arg("b"), pybind11::arg("alpha") = 0.85);
//    m.def("pdist",&pdist, py::arg("graphs"));
//    m.def("pdist2",&pdist2, py::arg().noconvert(), py::arg().noconvert());
    }

